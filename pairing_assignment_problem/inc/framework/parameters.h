#pragma once


#define MINUTES_PER_DAY (24*60)
#define TWELVE_HOURS (12*60)




namespace parameters
{
    const std::vector<std::string> bases { "CJA", "AHI"}; //all bases
    const int window_length = 31;
};

namespace segment_parameters
{



};


namespace duty_parameters
{
    const unsigned int limit_duty_time_per_duty = 14 * 60; // max allowed duty time per duty
    const unsigned int limit_blh_per_duty = 8 * 60; // max allowed block time per duty
    const unsigned int limit_deadheads_per_duty = 1; // max allowed number of deadheads
    const int max_consecutive_dhds = 1; 
    const int min_connection_time = 1 * 60 ;

};


namespace pairing_parameters
{
    const int min_rest_time_limit = 10 * 60; 
    const unsigned int max_pairing_length = 4 * MINUTES_PER_DAY;   
};


namespace cost_parameters
{
    const unsigned int limit_short_dhd_ = 2 * 60;
    const double basic_uncovered_segment_cost = 2048;
    const double basic_man_day_cost = 32;
    const double basic_layover_cost = 8;
    const double basic_dhd_long_cost = 8;
    const double basic_dhd_short_cost = 4;

};

namespace vertical_constraints_limits
{
	const std::map<std::string, int> limit_base_constraint = { std::pair<std::string, int>("CJA", 7),
    std::pair<std::string, int>("AHI", 7),
     };
}
