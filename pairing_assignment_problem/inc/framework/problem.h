#pragma once

#include <vector>

#include "framework/structs/segment.h"
#include "framework/structs/duty.h"
#include "framework/structs/pairing.h"

#include "framework/utils/io_segments.h"


class Problem
{

private:
    std::string _input_seg_filename;
    std::string _parameters_filename;

    std::vector<Segment *> _segments;
    std::vector<Segment *> _dhd_segments;

    Segment * _find_segment(size_t id, bool dhd=false) const
    {
        const std::vector<Segment *> * segments = dhd ? &_dhd_segments : &_segments;
        for(auto segment : *segments)
            if (segment->id() == id)
                return segment;
        return nullptr;
    }

public:

    Problem(std::string input_seg_filename = "")
        : _input_seg_filename(input_seg_filename) {

            if (!input_seg_filename.empty())
                read_segments(input_seg_filename);
        }

    bool read_segments(std::string input_seg_filename)
    {
        _segments = IOUtils::read_segments(input_seg_filename);
        for(auto _segment : _segments)
        {
            Segment *segment = new Segment(*_segment);
            segment->set_deadhead(true);
            _dhd_segments.push_back(segment);
        }

        return !_segments.empty();
    }

    bool read_parameters(std::string input_params_filename)
    {
        return true;
    }

    const std::vector<Segment *> & get_segments() const { return _segments; }
    const std::vector<Segment *> & get_dhd_segments() const { return _dhd_segments; }

    Segment *  get_oper_segment(size_t id) const { return _find_segment(id, false); }
    Segment *  get_dhd_segment(size_t id) const { return _find_segment(id, true); }
    Segment *  get_segment(size_t id, bool dhd=false) const { return _find_segment(id, dhd);}

    

    
};





