#pragma once

#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "framework/parameters.h"

#include "framework/structs/segment.h"
#include "framework/structs/pairing.h"
#include "framework/structs/duty.h"


namespace IOUtils
{

    std::vector<Segment *> read_segments(std::string filename)
    {
        std::vector<Segment *> segments;

        std::string line;
        std::ifstream file(filename);

        size_t id, onward_id;
        unsigned int dep_day, arr_day, dep_time, arr_time;
        std::string dep_stn, arr_stn;
        unsigned short flight_num;

        if (file.is_open())
        {
            while (getline (file, line))
            {
                std::stringstream ssline(line);

                ssline >> id;
                ssline >> flight_num;
                ssline >> dep_day;
                ssline >> arr_day;
                ssline >> dep_stn;
                ssline >> dep_time;
                ssline >> arr_stn;
                ssline >> arr_time;
                ssline >> onward_id;
                
                Segment * segment =
                    new Segment(id, 
                        flight_num,
                        dep_stn, arr_stn,
                        dep_day * MINUTES_PER_DAY + (dep_time / 100) * 60 + (dep_time % 100),
                        arr_day * MINUTES_PER_DAY + (arr_time / 100) * 60 + (arr_time % 100),
                        onward_id);

                segments.push_back(segment);
            }

            file.close();
        }

        return segments;
    };


    void write_pairings(std::string filename, const std::vector<Pairing *> & pairings)
    {

    };


    void print_pairings(const std::vector<Pairing *> & pairings)
    {
        for(int i = 0; i < pairings.size(); ++i)
        {
            std::cout << pairings[i]->get_string() << std::endl;
        }
    };


    std::vector<Pairing *> read_solution(std::string filename, const std::vector<Segment *>& segments, 
                                                                const std::vector<Segment *>& dhd_segments)
    {
        std::vector<Pairing *> pairings;

        std::string line, token;
        std::ifstream file(filename);

        Duty * duty = NULL;
        Segment * segment = NULL;
        Pairing * pairing = NULL;

        if (file.is_open())
        {
            while (getline (file, line))
            {
                if (line[0] == '#')
                    continue;

                std::stringstream sspairing(line);

                Pairing * pairing = new Pairing();

                while (getline (sspairing, token, ' '))
                {   
                    if (token == "[" || token == "]" || token == " ")
                    {
                        //pass
                    }
                    else if (token == "(")
                    {
                        duty = new Duty();
                    }
                    else if (token == ")")
                    {
                        pairing->add_duty(duty);
                        duty = NULL;
                    }
                    else
                    {
                        bool is_dhd = false;
                        size_t seg_id = 0;

                        if (token[token.size()-1] == 'D')
                        {
                            is_dhd = true;
                            token = token.substr(0, token.size()-1);
                        }

                        std::istringstream(token) >> seg_id;

                        if (is_dhd)
                        {
                            segment = dhd_segments[seg_id-1];
                        }
                        else
                        {
                            segment = segments[seg_id-1];
                        }
                        
                        duty->add_segment(segment);
                        segment = NULL;
                    }
                }
                
                pairings.push_back(pairing);
                pairing = NULL;               
            }

            file.close();
        }
        
        return pairings;
    };

}