#pragma once

#include "parameters.h"

#include "framework/structs/segment.h"
#include "framework/structs/duty.h"
#include "framework/structs/pairing.h"



class CostUtility
{
	
public:
	bool is_short_dhd(const Segment * const seg) const { 
		return seg->is_deadhead() && (seg->block_time() <= cost_parameters::limit_short_dhd_); }

	double cost_man_days(const Pairing * const pairing) const { return pairing->num_days() * cost_parameters::basic_man_day_cost; }
	double cost_layover(const Pairing * const pairing) const;
	double cost_dhd(const Pairing * const pairing) const;

};