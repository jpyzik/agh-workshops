#pragma once

#include <sstream>

// Time '0' (time_0) is defined as ...
// All other time-related data are defined as number of minutes from time '0'
// For instance if segment's departure and arrival times are equal to 
// respectively '720' and '870'. It means that flight departures at 
// "12:00" of the same day as time_0 and arrives at "14:30" of same day.
// More examples ...

class Segment
{
public:

	Segment(size_t id, 
		unsigned short flight_number,
		const std::string & dep_stn, const std::string & arr_stn,
		unsigned int dep_time, unsigned int arr_time,
		size_t onward_id
		)
		: _id(id)
		, _flight_number(flight_number)
		, _dep_stn(dep_stn)
		, _arr_stn(arr_stn)
		, _dep_time(dep_time)
		, _arr_time(arr_time)
		, _onward_id(onward_id)
		, _is_deadhead(false)
		, _is_covered(false)
	{}

	size_t id() const { return _id; }
	size_t onward_id() const { return _onward_id; }

	unsigned int dep_time() const { return _dep_time; }
	unsigned int arr_time() const { return _arr_time; }

	const std::string & dep_stn() const { return _dep_stn; }
	const std::string & arr_stn() const { return _arr_stn; }

	unsigned short flight_number() const { return _flight_number; }
	bool is_deadhead() const { return _is_deadhead; }
	void set_deadhead(bool dhd) {_is_deadhead = dhd; }
	bool is_covered() const { return _is_covered; }
	bool is_operating() const { return !_is_deadhead; }

	unsigned int block_time() const { return is_deadhead() ? 0 : _arr_time - _dep_time; }

	std::string get_string() const {
		std::stringstream str;
		str << id() << " "
			<< flight_number() << " "
			<< dep_stn() << " "
			<< dep_time() << " "
			<< arr_stn() << " "
			<< arr_time() << " "
			<< onward_id();
		return str.str();
	}

private:
	// Unique identifier
	size_t _id;

	// Onward segment identifer
	size_t _onward_id;

	// Flight number
	unsigned short _flight_number;

	// Segment departure station
	const std::string _dep_stn;

	// Segment arrival station
	const std::string _arr_stn;

	//UTC departure time
	const unsigned int _dep_time; 

	//UTC arrival time
	const unsigned int _arr_time; 

	bool _is_deadhead;
	bool _is_covered;
};
