#pragma once

#include "framework/structs/segment.h"


class Duty
{
public:

	const std::string & dep_stn() const { return segments_.front()->dep_stn(); }
	const std::string & arr_stn() const { return segments_.back()->arr_stn(); }

	unsigned int start_time() const { return segments_.front()->dep_time() - ci_time; }
	unsigned int end_time() const { return segments_.back()->arr_time() + co_time; }

	unsigned int duty_time() const { return end_time() - start_time(); }

	size_t num_segments() const { return segments_.size(); }
	void add_segment(Segment * seg) { segments_.push_back(seg); }
	Segment * get_segment(size_t pos) const { return segments_[pos]; }

	const std::vector<Segment *> & get_segments() const { return segments_; }

	unsigned int block_time() const {
		unsigned int res = 0;
		return std::accumulate(segments_.begin(), segments_.end(), res,
			[](unsigned int & res, Segment * seg) {return res + seg->block_time(); });
	}

	unsigned int  num_deadheads() const {
		using namespace std::placeholders;
		auto is_dhd = std::bind(&Segment::is_deadhead, _1);
		return std::count_if(segments_.begin(), segments_.end(), is_dhd);
	}

	std::string get_string() const {
		std::stringstream str;
		str << "( ";
		for (size_t i = 0; i < segments_.size(); ++i)
			str << segments_[i]->id() << (segments_[i]->is_deadhead() ? "D" : "") << " ";
		str << ")";
		return str.str();
	}

	
private:
	size_t id_;

	const unsigned short ci_time = 45; // Checkin time in minutes
	const unsigned short co_time = 15; // Checkout time in minutes 

	std::vector<Segment *> segments_;
};


