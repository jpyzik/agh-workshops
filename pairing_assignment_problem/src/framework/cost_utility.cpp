
#include "stdafx.h"
#include "framework/cost_utility.h"


double CostUtility::cost_layover(const Pairing * const pairing) const
{
	const size_t num_duties = pairing->num_duties();

	if (num_duties == 1)
		return 0.0;

	int total_layover_length = 0;

	const std::vector<Duty *> & duties = pairing->get_duties();
	std::vector<Duty *>::const_iterator curr_duty_itr = duties.begin(), next_duty_itr = duties.begin();
	const std::vector<Duty *>::const_iterator duty_end_itr = duties.end();
	++next_duty_itr;
	for (; next_duty_itr != duty_end_itr; ++curr_duty_itr, ++next_duty_itr) {
		const int layover_length = (*next_duty_itr)->start_time() - (*curr_duty_itr)->end_time();
		const int layover_length_as_12hrs_blocks = static_cast<int>(std::ceil(layover_length / TWELVE_HOURS));

		total_layover_length += layover_length_as_12hrs_blocks;
	}
	return total_layover_length * cost_parameters::basic_layover_cost;
}

double CostUtility::cost_dhd(const Pairing * const pairing) const
{
	int num_short_dhds = 0;
	int num_long_dhds = 0;
	for (auto duty : pairing->get_duties()) {
		for (auto segment : duty->get_segments()) {
			if (is_short_dhd(segment))
				num_short_dhds++;
			else if (segment->is_deadhead())
				num_long_dhds++;
		}
	}

	double total_cost
		= num_short_dhds * cost_parameters::basic_dhd_short_cost
		+ num_long_dhds * cost_parameters::basic_dhd_long_cost;

	return total_cost;
}