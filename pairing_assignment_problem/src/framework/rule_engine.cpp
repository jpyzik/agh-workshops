// RuleEngine.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "framework/rule_engine.h"

namespace
{
	bool is_joy_ride_after_adding_last_seg_(std::vector<const Segment *> & cons_dhd_seg)
	{
		const int numSegs = cons_dhd_seg.size();
		if (numSegs <= 1) return false;

		const std::string & last_arr = cons_dhd_seg.back()->arr_stn();

		for (const Segment * dhd_seg : cons_dhd_seg) {
			if (dhd_seg->dep_stn() == last_arr)
				return true;
		}

		return false;
	}
}


// rules
namespace rule_engine_duty_legality
{
	
	bool duty_time_per_duty(const Duty * const duty)
	{
		return duty->duty_time() <= duty_parameters::limit_duty_time_per_duty;
	}

	bool blh_per_duty(const Duty * const duty)
	{

		return duty->block_time() <= duty_parameters::limit_blh_per_duty;
	}

	bool connection_time(const Duty * const duty)
	{
		bool legal = true;

		std::vector<Segment *>::const_iterator
			it = duty->get_segments().begin(),
			it_next = duty->get_segments().begin(),
			it_end = duty->get_segments().end();

		// point to the next segment

		for (; it != it_end && legal; ++it) {
			
			if (++it_next == it_end)
				break;

			if ((*it)->onward_id() != (*it_next)->id()) { 
				int connection_tm = (*it_next)->dep_time() - (*it)->arr_time();

				legal = duty_parameters::min_connection_time <= connection_tm;					
			}
		}

		return legal;
	}

	bool deadheads_per_duty(const Duty * const duty)
	{
		return duty->num_deadheads() <= duty_parameters::limit_deadheads_per_duty;
	}

	bool consecutive_deadheads(const Duty * const duty)
	{
		bool legal = true;
		int cons_deadheads = 0;

		for (Segment * seg : duty->get_segments()) {
			if (seg->is_deadhead())
				cons_deadheads++;
			else
				cons_deadheads = 0;

			if (cons_deadheads > duty_parameters::max_consecutive_dhds) {
				legal = false;
				break;
			}
		}

		return legal;
	}

	bool joy_ride(const Duty * const duty)
	{
		const std::vector<Segment *> & segments = duty->get_segments();
		std::vector<const Segment *> cons_dhd_seg;
		cons_dhd_seg.reserve(5);

		for (const Segment * seg : segments) {
			if (seg->is_deadhead()) {
				cons_dhd_seg.push_back(seg);
				bool is_joy_ride = is_joy_ride_after_adding_last_seg_(cons_dhd_seg);
				if (is_joy_ride)
					return false;
			}
			else {
				cons_dhd_seg.clear();
			}
		}

		return true;
	}
}

namespace rule_engine_pairing_legality
{
	
	bool pairing_days(const Pairing * const pairing)
	{
		return pairing->pairing_length() < pairing_parameters::max_pairing_length;
	}

	// Minimum rest after duty is maximum of either 08:00 or length  of duty.
	// Ex.:
	//    * Duty that lasts 06:00 has minimum rest requiered after equal to 08:00.
	//    * Duty that lasts 10:00 has minimum rest requiered after equal to 10:00.
	bool min_rest_time(const Pairing * const pairing)
	{
		bool legal = true;

		std::vector<Duty *>::const_iterator
			it = pairing->get_duties().begin(),
			it_next = pairing->get_duties().begin(),
			it_end = pairing->get_duties().end();

		++it_next; // point to the next segment
		for (; it != it_end && legal; ++it) {
			if (it_next != it_end){
				// continuity in space and time
				const int rest_time = (*it_next)->start_time() - (*it)->end_time();
				const int rest_time_limit = std::max<int>(pairing_parameters::min_rest_time_limit, (*it)->duty_time());
				legal = (rest_time >= rest_time_limit);

				++it_next;
			}
		}

		return legal;
	}

	// here only check across duties is performed
	bool joy_ride(const Pairing * const pairing)
	{
		const std::vector<Duty *> & duties = pairing->get_duties();
		const int num_duties = duties.size();
		if (num_duties <= 1)
			return true;

		std::vector<const Segment *> cons_dhd_seg;
		cons_dhd_seg.reserve(5);
		bool acrossDuties = false;

		for (const Duty * duty : duties) {
			const std::vector<Segment *> & segments = duty->get_segments();
			for (const Segment * seg : segments) {
				if (seg->is_deadhead()) {
					cons_dhd_seg.push_back(seg);
					bool is_joy_ride = is_joy_ride_after_adding_last_seg_(cons_dhd_seg);
					if (is_joy_ride)
						return false;
				}
				else {
					cons_dhd_seg.clear();
				}
			}
		}

		return true;
	}

}



// This is the constructor of a class that has been exported.
// see RuleEngine.h for the class definition
RuleEngine::RuleEngine()
{
	return;
}

// Basic segment validity check.
// Segment is considered as valid if the following conditions are met:
//  * it does *not* starts and ends at the same station
//  * arrival time is later than departure time
bool RuleEngine::is_valid(const Segment * const segment) const
{
	return 
		segment->dep_stn() != segment->arr_stn() 
		&& segment->dep_time() < segment->arr_time();
}

// Basic duty validity check
// Duty is considered as valid if the following conditions are met:
//  * all segments are valid
//  * each pair of consecutive segments is continues in space and time:
//    which means that arrival station of the later segment is equal to departure station of the former
//    and arrival time of the later segment is (strictly) earlier than departure time of the former
bool RuleEngine::is_valid(const Duty * const duty) const
{
	bool valid = true;

	if (duty->get_segments().empty())
		return false;

	std::vector<Segment *>::const_iterator
		it = duty->get_segments().begin(),
		it_next = duty->get_segments().begin(),
		it_end = duty->get_segments().end();

	++it_next; // point to the next segment
	for (; it != it_end && valid; ++it) {
		valid = is_valid(*it);

		if (valid && it_next != it_end){

			valid = (*it)->arr_stn() == (*it_next)->dep_stn()
				&& (*it)->arr_time() <= (*it_next)->dep_time();

			++it_next;
		}
	}

	return valid;
}

// Basic pairing validity check
// Pairing is considered as valid if the following conditions are met:
//  * all duties are valid
//  * each pair of consecutive duties is continues in space and time:
//    which means that arrival station of the later duty is equal to departure station of the former
//    and arrival time of the later duty is (strictly) earlier than departure time of the former
//  * pairing starts and ends at the same station called pairing base. List of allowed pairing bases is predefined
//  * pairing cannot have layover (rest time between duties) at pairing base
bool RuleEngine::is_valid(const Pairing * const pairing) const
{
	bool valid = true;

	if (pairing->get_duties().empty())
		return false;

	const std::vector<std::string> & bases = parameters::bases;

	valid = (pairing->get_duties().front()->dep_stn() == pairing->get_duties().back()->arr_stn());
	bool is_base = std::find(bases.begin(), bases.end() , pairing->base()) != bases.end();
	
	valid = valid && is_base;

	std::vector<Duty *>::const_iterator
		it = pairing->get_duties().begin(),
		it_next = pairing->get_duties().begin(),
		it_end = pairing->get_duties().end();

	++it_next; // point to the next duty
	for (; it != it_end && valid; ++it) {
		valid = is_valid(*it);

		
		if (valid && it_next != it_end){

			// continuity in space and time
			valid = valid && (*it)->arr_stn() == (*it_next)->dep_stn()
				&& (*it)->end_time() < (*it_next)->start_time();

			++it_next;
		}
	}

	return valid;
}

bool RuleEngine::is_legal(const Duty * const duty) const
{
	using namespace rule_engine_duty_legality;
	
	bool legal
		=  blh_per_duty(duty)
		&& connection_time(duty)
		&& deadheads_per_duty(duty)
		&& consecutive_deadheads(duty)
		&& duty_time_per_duty(duty)
		&& joy_ride(duty);

	return legal;
}

bool RuleEngine::is_legal(const Pairing * const pairing) const
{
	bool legal = true;
	for (const Duty * const duty : pairing->get_duties()) {
		legal = is_legal(duty);
	}
    
	if (!legal)
		return legal;
		
	using namespace rule_engine_pairing_legality;
	
	legal
		= min_rest_time(pairing)
		&& pairing_days(pairing)
		&& joy_ride(pairing);

	return legal;
}

